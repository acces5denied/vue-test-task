export default function endingFormat (value, text) {
  function ending (n, text) {
    n = Math.abs(n) % 100; var n1 = n % 10
    if (n > 10 && n < 20) return `${n} ${text[2]}`
    if (n1 > 1 && n1 < 5) return `${n} ${text[1]}`
    if (n1 === 1) return `${n} ${text[0]}`
    return `${n} ${text[2]}`
  }
  return ending(value, text)
}
