export default function priceFormat (value) {
  return (parseFloat(value) + ' ₽').replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ')
}
