import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    loader: true,
    products: [],
    perPage: 6,
    curentPage: 1,
    pages: 1,
    riskRatio: '',
    processProducts: []
  },
  mutations: {
    SET_PRODUCTS_TO_STATE_PRODUCTS (s, data) {
      s.products = data
    },
    SET_PRODUCTS_PROCESS (s, page) {
      s.loader = true
      // фильтруем products
      const products = s.products.filter(product => {
        if (s.riskRatio) {
          return product.risk_ratio === s.riskRatio
        }
        return product
      })
      // передаем текущую страницу
      s.curentPage = page
      // определяем кол-во страниц
      s.pages = Math.ceil(products.length / s.perPage)
      // разбиваем products на страницы
      const from = (s.curentPage - 1) * s.perPage
      const to = from + s.perPage
      s.processProducts = products.slice(from, to)
      s.loader = false
    },
    SET_RISK_RATIO (s, ratio) {
      s.riskRatio = ratio
    }
  },
  actions: {
    async GET_PRODUCTS_FROM_API ({ commit }) {
      try {
        axios.get('/products.json').then(response => {
          // передаем products
          commit('SET_PRODUCTS_TO_STATE_PRODUCTS', response.data)
          // обрабатываем массив products, сначала фильтруем потом разбиваем на страницы
          commit('SET_PRODUCTS_PROCESS', 1)
        })
      } catch (e) {
        throw e
      }
    },
    CHANGE_PAGE ({ commit }, page) {
      // меняем страницу в пагинации
      commit('SET_PRODUCTS_PROCESS', page)
    },
    FILTERED ({ commit }, ratio) {
      // меняем значение для фильтра
      commit('SET_RISK_RATIO', ratio)
      // запускаем фильтр
      commit('SET_PRODUCTS_PROCESS', 1)
    }
  },
  getters: {
  },
  modules: {

  }
})
